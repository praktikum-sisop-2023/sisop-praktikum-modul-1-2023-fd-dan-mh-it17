# Sisop-Praktikum-Modul-1-2023-MHFD-IT17

Nama kelompok : 
1. Salmaa Satifha Dewi Rifma Putri  (5027211011)
2. Yoga Hartono                     (5027211023)
3. Dzakirozaan Uzlahwasata          (5027211066)

# Soal 1
## Analisa Soal
• Menampilkan 5 Universitas dengan ranking tertinggi di Jepang  
• Menampilkan Universitas di Jepang yang memiliki Faculty Student Score(fsr score) yang paling rendah   
• Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.  
• Menampilkan Universitas paling keren di dunia dengan kata kunci keren

## Cara Pengerjaan Soal 1
• Menampilkan 5 Universitas dengan ranking tertinggi di Jepang
```sh
#!/bin/bash
grep "Japan" univ_rank.csv | sort -t , -k1 -g | head -5 | awk -F , '{print $1,$2}'
```
- gunakan `grep` untuk mencari kata "Japan" di dalam file `univ_rank.csv`.
- gunakan `sort -t , -k1 -g` untuk mensortir pada kolom 1, yaitu kolom rank keseluruhan dengan command `-k1`, untuk `-g` sendiri berfungsi untuk mengurutkan numerical yang bersifat nomor yang banyak seperti 600+
- gunakan `head -5` untuk print 5 baris pertama yang sudah di sort
- gunakan `awk -F , '{print $1,$2}'` untuk print hanya kolom 1 dan 2 saja

• Menampilkan Universitas di Jepang yang memiliki Faculty Student Score(fsr score) yang paling rendah
```sh
grep "Japan" univ_rank.csv | sort -t , -k9 -g | awk -F , '{print $1,$2,$9}' | head -5
```
- gunakan `-k9` untuk sort kolom 9, yaitu kolom yang berisi FSR Score
- gunakan `awk -F , '{print $1,$2,$9}'` untuk print kolom 1, 2, dan 9 saja
- gunakan `head -5` untuk print 5 baris pertama yang sudah di sort

• Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```sh
grep "Japan" univ_rank.csv | sort -t , -k20 -g | awk -F , '{print $1,$2,$20}' | head -10
```
- gunakan `-k20` untuk sort kolom 20, yaitu kolom yang berisi GER Rank
- gunakan `awk -F , '{print $1,$2,$20}'` untuk print kolom 1, 2, dan 9 saja
- gunakan `head -10` untuk print 10 baris pertama yang sudah di sort

• Menampilkan Universitas paling keren di dunia dengan kata kunci keren
```sh
grep -i "keren" univ_rank.csv | awk -F , '{print $1,$2}'
```
- gunakan `-i` untuk mencari kata "keren" pada file `univ_rank.csv`

## Source Code
```sh
echo "|5 Universitas dengan ranking tertinggi di Jepang|\n"

grep "Japan" univ_rank.csv | sort -t , -k1 -g | head -5 | awk -F , '{print $1,$2}'
echo
echo "|Universitas dengan FSR Score terendah di Jepang|\n"

grep "Japan" univ_rank.csv | sort -t , -k9 -g | awk -F , '{print $1,$2,$9}' | head -5
echo
echo "|10 Universitas di Jepang dengan GER Rank paling tinggi|\n"

grep "Japan" univ_rank.csv | sort -t , -k20 -g | awk -F , '{print $1,$2,$20}' | head -10
echo
echo "|Universitas paling keren di dunia|\n"

grep -i "keren" univ_rank.csv | awk -F , '{print $1,$2}'
```

## Test Output
![output-modul1](https://i.ibb.co/W0wcR3G/Whats-App-Image-2023-03-03-at-21-44-08.jpg)

# Soal 2
## Analisa Soal
- Mendownload gambar tentang Indonesia sebanyak X kali dengan X sebagai jam sekarang dengan ketentuan : 
    - jika jam 0 mendownload 1 gambar saja
    - didownload 10 jam sekali
    - file bernama "perjalanan_nomor.file" (urutan file terdownload)
- File gambar dimasukkan dalam folder bernama "kumpulan_nomor.folder” (urutan folder terdownload)
- Mengubah format folder kumpulan menjadi zip dengan ketentuan :
    - terdownload ekstensi zip setiap 1 hari 
    - format nama zip “devil_nomor.zip” (urutan folder saat dibuat) 

## Cara Pengerjaan Soal 2
• Membuat folder soal2
```sh
mkdir soal2
```
• 2. Membuat dan membuka file script .sh dengan format nama kobeni_liburan.sh.
```sh
nano kobeni_liburan.sh
```
• Menentukan shell yang digunakan
```sh
#!/bin/bash
```
- baris pertama script ini menentukan shell yang akan digunakan untuk mengeksekusi script.

• Mencari waktu realtime
```sh
jam=$(date +"%H")
```
- baris ini menyimpan waktu real-time pada variabel jam yang dideklarasikan dengan format waktu `%H`(format 24 jam).

• Menentukan jumlah gambar yang didownload
```sh
# Mengecek jika saat ini pukul 00.00 (dini hari)
if [[ "$jam" -eq 0 ]]; then
  # Set gambar yang didownload dengan jumlah = 1
	downloads=1
else
  # Set gambar yang didownload dengan jumlah sesuai jam yang real time yang sudah dideklarasi
	downloads="$jam"
fi
```
- `if [[ "$jam" -eq 0 ]]; then`: ini adalah sebuah statement percabangan if. Jika jam sama dengan 0, maka akan dieksekusi perintah dalam blok if.
- `downloads=1`: jika saat ini pukul 00.00 (dini hari), downloads akan diset dengan angka 1.
- `else`: Jika jam tidak sama dengan 0, maka eksekusi akan dilanjutkan ke perintah yang ada dalam blok else.
- `downloads="$jam"`: di sini, downloads akan diset dengan angka yang sama dengan jam yang sedang berjalan.

• Membuat direktori untuk menyimpan hasil download an gambar
```sh
if [[ "$1" == "download" ]]; then

  # Deklarasi direktori baru
	direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
 	mkdir "$direktori"
```
• Mendownload gambar dari link
```sh
	for ((i = 1;i <= downloads; i++)); do
		file="perjalanan_$i"
 		wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"  		
	done
```
- `if [[ "$1" == "download" ]]; then`: ini adalah statement percabangan if lainnya. Ini memeriksa apakah argumen pertama yang diberikan saat menjalankan script adalah download.
- `direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"`: ini menyimpan string `"kumpulan_"` ditambah dengan nomor direktori baru di direktori. Nomor direktori baru dihitung dengan menjumlahkan 1 dengan jumlah direktori yang telah ada sebelumnya.
- `mkdir "$direktori"`: membuat direktori baru sesuai dengan nama yang didefinisikan dalam variabel `$direktori`.
- `for ((i = 1;i <= downloads; i++)); do`: perulangan for dimulai dengan menginisialisasi i dengan angka 1, dan dilakukan selama i kurang dari atau sama dengan jumlah downloads. i akan bertambah 1 setiap kali perulangan dilakukan.
- `file="perjalanan_$i"`: nama file yang disimpan dalam variabel file.
- `wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"`: ini mendownload gambar dari URL yang diberikan dengan menggunakan perintah wget. File disimpan dengan nama `perjalanan_$i` di direktori yang telah dibuat sebelumnya.

• Mengubah ekstensi folder yang sudah tersimpan menjadi format zip
```sh
elif [[ "$1" == "zip" ]]; then

# Mencari semua kumpulan direktori yang sudah ada
kumpdirektori=$(find . -type d -name "kumpulan_*")

# Membuat zip file dan dipindah ke direktori
zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"
zip -r "$zipfile" $kumpdirektori

else
# Menampilkan pesan dan keluar
echo "Usage: $0 [download|zip]"
exit 1
fi
```
- `elif [[ "$1" == "zip" ]]; then`: ini adalah statement percabangan elif. Jika argumen pertama yang diberikan saat menjalankan script adalah zip, maka perintah dalam blok elif akan dieksekusi.
- `kumpdirektori=$(find . -type d -name "kumpulan_*")`: mencari semua direktori yang telah dibuat sebelumnya dengan nama yang dimulai dengan "kumpulan_".
- `zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"`: nama file zip baru disimpan dalam variabel zipfile. Nama file tersebut diawali dengan "devil_", dan diikuti dengan nomor urut yang
berurutan dari 1,2,3 dan seterusnya.

• Membuat cron job
```sh
crontab -e
```
- membuat cron job dengan mengetik `crontab -e` pada terminal
- `crontab`: program untuk menjalankan tugas pada waktu tertentu secara otomatis
- `e`: opsi mengedit atau mengubah jadwal tugas yang sudah ada ataupun belum

• Me-set waktu yang digunakan untuk mendownload gambar dan mengubah menjadi format zip pada crontab
```sh
# comment perintah konfigurasi pada crontab
 * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download
 0 0 * * *  \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip
```
`- * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download`
Perintah ini akan dijalankan setiap 10 jam sekali, dimulai dari menit ke-0 setiap jamnya. Kemudian, `\\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download` merujuk pada file shell script kobeni_liburan.sh yang terletak pada direktori /home/alma/soal2 dan dengan argumen download.

`0 0 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip`
Perintah ini akan dijalankan setiap hari pukul 00:00. Kemudian, `\\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip` merujuk pada file shell script kobeni_liburan.sh yang terletak pada direktori /home/alma/soal2 dan dengan argumen zip.

Keduanya merujuk pada file shell script yang sama (`kobeni_liburan.sh`) yang akan dijalankan pada waktu yang sudah ditentukan untuk melakukan tugas-tugas tertentu, yaitu download gambar dan zip folder yang sudah dibuat oleh script.

## Source Code
```sh
#!/bin/bash

jam=$(date +"%H")

if [[ "$jam" -eq 0 ]]; then
	downloads=1
else
	downloads="$jam"
fi

if [[ "$1" == "download" ]]; then

	direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
 	mkdir "$direktori"

	for ((i = 1;i <= downloads; i++)); do
		file="perjalanan_$i"
 		wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"  		
	done

elif [[ "$1" == "zip" ]]; then

kumpdirektori=$(find . -type d -name "kumpulan_*")

zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"
zip -r "$zipfile" $kumpdirektori

else
echo "Usage: $0 [download|zip]"
exit 1
fi
```
```sh
 * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download
 0 0 * * *  \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip
```
## Test Ouput
- Ouput mendownload file gambar dan disimpan pada folder    
![ouput21-modul1](https://i.ibb.co/yRnn72H/output-download.png)

- Output mengubah ekstensi folder menjadi zip   
![output22-modul1](https://i.ibb.co/Cnh9PhT/output-zip.png)

## Kendala yang dialami 
1. Menemui kendala syntax error, kesalahan deklarasi variabel, dan kesalahan pemberian spasi pada code. Namun kendala sudah diatasi dengan melakukan perbaikan pada code. 
2. Menemui kendala hasil output “value too great for base (error token is "09")” ketika di bash, namun sudah diperbaiki dengan perbaikan code perintah pada crontab


# Soal 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username 
    - Tidak boleh menggunakan kata chicken atau ernie
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

## Analisa Soal
- Membuat sistem register di dalam script louis.sh
- Membuat sistem login di dalam script retep.sh
- User yang telah melakukan register dan login, tersimpan dalam log.txt yang berada pada ./users/users.txt beserta tanggal dan informasinya

### Cara Pengerjaan Soal 3
- Membuat folder soal3
- Membuat log.txt didalam soal3
- Membuat folder users
- Membuat folder users.txt didalam folder users

Pengerjaan louis.sh:
- Buka terminal
- cd soal3
- nano louis.sh

- **louis.sh**

- Menampilkan Waktu dan Tanggal
```sh 
date=$(date +"%y/%m/%d %H:%M:%S")
```

- Memasukkan Username
```sh
read -p "Enter username: " username
```

- Membuat Password

- Kriteria 8 Karakter (Minimal terdiri dari 8 karakter)
```sh
function checkLength(){
    if [ "${#password}" -ge 8 ]; then
        checkUsername
    else  
        echo "Password must be at least 8 characters long!"
    fi
}
```

- Kriteria Kapital (Minimal terdapat 1 huruf kapital dan 1 huruf kecil)
```sh 
function checkCase(){
    if [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]]; then
        checkAlphanumeric
    else
        echo "Password must contain at least one uppercase letter and one lowercase letter!"
    fi
}
```

- Kriteria Angka (Minimal terdapat 1 angka)
```sh
function checkAlphanumeric(){
    if [[ "$password" =~ [0-9] ]]; then
        echo "Username: ${username}" >> /home/kali/soal3/users/users.txt
        echo "Password: ${password}" >> /home/kali/soal3/users/users.txt
          status="$date REGISTER: INFO User $username registered successfully"
        echo "$status" >> /home/kali/soal3/log.txt
    else
        echo "Password must contain at least one digit!"
    fi
}
```

- Kriteria Password != Username (Password tidak boleh sama dengan Username)
```sh
function checkUsername(){
    if [ "$password" != "$username" ]; then
        checkChickenernie
    else
        echo "Password cannot contain the username"
    fi
}
```

- Kriteria Chicken dan Ernie (Password tidak boleh mengandung kata chicken atau ernie)
- grep -q -1 disini digunakan untuk case insensitive dalam kata chicken atau ernie 
```sh
function checkChickenernie(){
    if  echo "$password" | grep -q -i 'chicken\|ernie'; then                                     
        echo "Password cannot contain the words 'chicken' or 'ernie'!"      
    else
        checkCase
    fi
}
```

- Kriteria Username Telah Terdaftar Pada /home/kali/soal3/users/users.txt
```sh
if grep -q "^Username: $username$" /home/kali/soal3/users/users.txt; then
    status="$date REGISTER: ERROR User already exists"
    echo "$status" >> /home/kali/soal3/log.txt 

else 
```

- Memasukkan Password
```sh
read -s -p  "Enter Password: " password
    checkLength
fi

echo "$status"
```

Pengerjaan retep.sh
- Buka terminal
- cd soal3
- nano retep.sh

- **retep.sh**

- Masukkan Username
```sh
read -p "Enter username: " username
```

- Cek Apabila Username Sudah Ada Di home/kali/soal3/users/users.txt

```sh
if ! grep -qw "$username" /home/kali/soal3/users/users.txt; then
  echo "User does not exist"
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>/home/kali/soal3/log.txt
  exit 1
fi
```

- Masukkan Password
```sh
read -s -p "Enter password: " password
```

- Mengambil Password Untuk Username Yang Di Masukkan Dari home/kali/soal3/users/users.txt
```sh
saved_password=$(grep -w -A 2 "$username" /home/kali/soal3/users/users.txt | sed -n 2p | cut -d " " -f 2)
```

- Cek Apabila Password Sudah Sama Seperti Yang Tersimpan Di Saved Password
```sh
if [ "$password" != "$saved_password" ]; then
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username "
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> /home/kali/soal3/log.txt
  exit 1
else
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in"
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> /home/kali/soal3/log.txt
  exit 0
fi
```
### Source Code

- louis.sh
```sh
#!/bin/bash

date=$(date +"%y/%m/%d %H:%M:%S")


read -p "Enter username: " username


function checkLength(){
    if [ "${#password}" -ge 8 ]; then
        checkUsername
    else  
        echo "Password must be at least 8 characters long!"
    fi
}


function checkCase(){
    if [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]]; then
        checkAlphanumeric
    else
        echo "Password must contain at least one uppercase letter and one lowercase letter!"
    fi
}


function checkAlphanumeric(){
    if [[ "$password" =~ [0-9] ]]; then
        echo "Username: ${username}" >> /home/kali/soal3/users/users.txt
        echo "Password: ${password}" >> /home/kali/soal3/users/users.txt
          status="$date REGISTER: INFO User $username registered successfully"
        echo "$status" >> /home/kali/soal3/log.txt
    else
        echo "Password must contain at least one digit!"
    fi
}


function checkUsername(){
    if [ "$password" != "$username" ]; then
        checkChickenernie
    else
        echo "Password cannot contain the username"
    fi
}


function checkChickenernie(){
    if  echo "$password" | grep -q -i 'chicken\|ernie'; then                                     
        echo "Password cannot contain the words 'chicken' or 'ernie'!"      
    else
        checkCase
    fi
}


if grep -q "^Username: $username$" /home/kali/soal3/users/users.txt; then
    status="$date REGISTER: ERROR User already exists"
    echo "$status" >> /home/kali/soal3/log.txt 

else 

read -s -p  "Enter Password: " password
    checkLength
fi

echo "$status"
```

- retep.sh
```sh
#!/bin/bash


read -p "Enter username: " username


if ! grep -qw "$username" /home/kali/soal3/users/users.txt; then
  echo "User does not exist"
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>/home/kali/soal3/log.txt
  exit 1
fi


read -s -p "Enter password: " password

saved_password=$(grep -w -A 2 "$username" /home/kali/soal3/users/users.txt | sed -n 2p | cut -d " " -f 2)

if [ "$password" != "$saved_password" ]; then
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username "
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> /home/kali/soal3/log.txt
  exit 1
else
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in"
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> /home/kali/soal3/log.txt
  exit 0
fi

```

## Test Ouput
- louis berhasil
![louisSuccess](/uploads/d5c2ccd7ebc001526e9a1ec45d303d96/Screenshot_2023-03-10_002458.png)
- louis gagal
![louisFailed](/uploads/08ef4973592ee98c6bcb9f81e0448767/Screenshot_2023-03-10_002940.png)
- retep berhasil
![retepSuccess](/uploads/11298b44bc6b02cc8822f60703e72003/image.png)
- retep gagal
![retepFailed](/uploads/21c5a1c8da7b8ed8ef6b9228c9b4fbd6/image.png)


# Soal 4
## Analisa Soal
• Membuat backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt)    
• Isi file backup log system dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup, seperti berikut: 
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14 
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya
- Setelah huruf z akan kembali ke huruf a   
• Membuat script untuk dekripsi file backup yang sudah di enkripsi  
• Membuat script untuk auto backup file syslog setiap 2 jam

## Cara Pengerjaan Soal 4
• Pengerjaan script enkripsi
```sh
#!/bin/bash

date=$(date +"%H:%M %d:%m:%Y")
```
- gunakan `date=$(date +"%H:%M %d:%m:%Y")` untuk Mengatur variabel date dengan waktu saat ini dalam format HH:MM dd:MM:YYYY

```sh
filename=$(date +"%H:%M %d:%m:%Y").txt
```
- gunakan `filename=$(date +"%H-%M_%d-%m-%Y").txt` untuk mengaliaskan filename sebagai data waktu jam dan juga tanggal sekarang pada device

```sh
while read line; do
```
- Membuka file syslog dan membaca setiap baris dalam file tersebut

```sh
encrypted_line=""
```
- Mengatur variabel encrypted_line untuk menyimpan isi baris yang sudah dienkripsi

```sh
for (( i=0; i<${#line}; i++ )); do
```
- Melakukan loop untuk setiap karakter dalam baris

```sh
char=${line:i:1}
```
- Mengambil karakter pada indeks i dari baris

```sh
if [[ $char =~ [a-zA-Z] ]]; then
```
- Jika karakter merupakan huruf alfabet

```sh
ascii_val=$(printf '%d' "'$char")
```
- Mendapatkan nilai ASCII dari karakter tersebut

```sh
if [[ $char =~ [a-z] ]]; then
```
- Jika karakter adalah huruf kecil

```sh
encrypted_val=$(( (ascii_val - 97 + ${#date} + 1) % 26 + 97 ))
else
```
- Menghitung nilai ASCII setelah dienkripsi

```sh
encrypted_val=$(( (ascii_val - 65 + ${#date} + 1) % 26 + 65 ))
fi
```
- Menghitung nilai ASCII setelah dienkripsi

```sh
encrypted_char=$(printf "\$(printf '%03o' "$encrypted_val")")
```
- Mengubah nilai ASCII menjadi karakter yang sesuai

```sh
encrypted_line="$encrypted_line$encrypted_char"
else
```
- Menyimpan karakter yang sudah dienkripsi ke variabel encrypted_line

```sh
encrypted_line="$encrypted_line$char"
fi
```
- Jika karakter bukan huruf alfabet, langsung disimpan ke variabel encrypted_line

```sh
echo $encrypted_line >> /home/kali/Desktop/$filename
```
- Menyimpan isi baris yang sudah dienkripsi ke file dengan nama yang sama dengan file backup yang sudah dibuat sebelumnya

```sh
done < /var/log/syslog
```
- Mengambil input dari file syslog

• Pengerjaan script dekripsi
```sh
decrypted_val=$(( (ascii_val - 97 - ${#date} - 1 + 26) % 26 + 97 ))
else
decrypted_val=$(( (ascii_val - 65 - ${#date} - 1 + 26) % 26 + 65 ))
fi
```
- Untuk logikanya mirip seperti enkripsi tetapi hanya berbeda dalam penggunaan `+` `-` nya saja, karena dekripsi adalah pengembalian menjadi asalnya sehingga menggunakan `-`

## Source Code
# log_encrypt.sh
```sh
#!/bin/bash

date=$(date +"%H:%M %d:%m:%Y")
filename=$(date +"%H:%M %d:%m:%Y").txt

while read line; do
    encrypted_line=""
    for (( i=0; i<${#line}; i++ )); do
        char=${line:i:1}
        if [[ $char =~ [a-zA-Z] ]]; then
            ascii_val=$(printf '%d' "'$char")
            if [[ $char =~ [a-z] ]]; then
                encrypted_val=$(( (ascii_val - 97 + ${#date} + 1) % 26 + 97 ))
            else
                encrypted_val=$(( (ascii_val - 65 + ${#date} + 1) % 26 + 65 ))
            fi
            encrypted_char=$(printf "\\$(printf '%03o' "$encrypted_val")")
            encrypted_line="$encrypted_line$encrypted_char"
        else
            encrypted_line="$encrypted_line$char"
        fi
    done
    echo $encrypted_line >> /home/kali/Desktop/$filename
done < /var/log/syslog

echo "File syslog telah berhasil dicatat dan dienkripsi."

# crontab -e
# 0 */2 * * *      /home/kali/Desktop/log_encrypt.sh
```
# log_decrypt.sh
```sh
#!/bin/bash

date=$(date +"%H:%M %d:%m:%Y")
filename=$(date +"%H:%M %d:%m:%Y").txt

while read line; do
    decrypted_line=""
    for (( i=0; i<${#line}; i++ )); do
        char=${line:i:1}
        if [[ $char =~ [a-zA-Z] ]]; then
            ascii_val=$(printf '%d' "'$char")
            if [[ $char =~ [a-z] ]]; then
                decrypted_val=$(( (ascii_val - 97 - ${#date} - 1 + 26) % 26 + 97 ))
            else
                decrypted_val=$(( (ascii_val - 65 - ${#date} - 1 + 26) % 26 + 65 ))
            fi
            decrypted_char=$(printf "\\$(printf '%03o' "$decrypted_val")")
            decrypted_line="$decrypted_line$decrypted_char"
        else
            decrypted_line="$decrypted_line$char"
        fi
    done
    echo "$decrypted_line" >> "${filename%.*}_decrypted.txt"
done < /home/kali/Desktop/$filename

echo "File $filename telah berhasil didekripsi."
```

## Test Output
• Output file syslog yang telah dienkripsi  
![output-log_encrypt.sh](https://i.ibb.co/QPCXJ6M/Screenshot-2023-03-07-224517.png)

• Output file syslog yang telah didekripsi  
![output-log_decrypt.sh](https://i.ibb.co/1QzgYts/Screenshot-2023-03-07-224751.png)
