#!/bin/bash

echo "|5 Universitas dengan ranking tertinggi di Jepang|\n"

grep "Japan" univ_rank.csv | sort -t , -k1 -g | head -5 | awk -F , '{print $1,$2}'
echo
echo "|Universitas dengan FSR Score terendah di Jepang|\n"

grep "Japan" univ_rank.csv | sort -t , -k9 -g | awk -F , '{print $1,$2,$9}' | head -5
echo
echo "|10 Universitas di Jepang dengan GER Rank paling tinggi|\n"

grep "Japan" univ_rank.csv | sort -t , -k20 -g | awk -F , '{print $1,$2,$20}' | head -10
echo
echo "|Universitas paling keren di dunia|\n"

grep -i "keren" univ_rank.csv | awk -F , '{print $1,$2}'