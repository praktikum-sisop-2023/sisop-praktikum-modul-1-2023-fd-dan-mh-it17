#!/bin/bash

# Menampilkan Waktu dan Tanggal

date=$(date +"%y/%m/%d %H:%M:%S")

# Masukkan Username

read -p "Enter username: " username

# Kriteria Password

# Kriteria 8 Karakter

function checkLength(){
    if [ "${#password}" -ge 8 ]; then
        checkUsername
    else  
        echo "Password must be at least 8 characters long!"
    fi
}

# Kriteria Kapital

function checkCase(){
    if [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]]; then
        checkAlphanumeric
    else
        echo "Password must contain at least one uppercase letter and one lowercase letter!"
    fi
}

# Kriteria Angka

function checkAlphanumeric(){
    if [[ "$password" =~ [0-9] ]]; then
        echo "Username: ${username}" >> /home/kali/soal3/users/users.txt
        echo "Password: ${password}" >> /home/kali/soal3/users/users.txt
          status="$date REGISTER: INFO User $username registered successfully"
        echo "$status" >> /home/kali/soal3/log.txt
    else
        echo "Password must contain at least one digit!"
    fi
}

# Kriteria Password != Username

function checkUsername(){
    if [ "$password" != "$username" ]; then
        checkChickenernie
    else
        echo "Password cannot contain the username"
    fi
}

# Kriteria Chicken dan Ernie

function checkChickenernie(){
    if  echo "$password" | grep -q -i 'chicken\|ernie'; then                                     
        echo "Password cannot contain the words 'chicken' or 'ernie'!"      
    else
        checkCase
    fi
}

# Kriteria Username Telah Terdaftar Pada /home/kali/soal3/users/users.txt

if grep -q "^Username: $username$" /home/kali/soal3/users/users.txt; then
    status="$date REGISTER: ERROR User already exists"
    echo "$status" >> /home/kali/soal3/log.txt 

else 

read -s -p  "Enter Password: " password
    checkLength
fi

echo "$status"
