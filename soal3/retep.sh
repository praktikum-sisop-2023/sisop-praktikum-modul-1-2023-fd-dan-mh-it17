#!/bin/bash

# Masukkan Username

read -p "Enter username: " username

# Cek Apabila Username Sudah Ada Di home/kali/soal3/users/users.txt

if ! grep -qw "$username" /home/kali/soal3/users/users.txt; then
  echo "User does not exist"
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>/home/kali/soal3/log.txt
  exit 1
fi

# Masukkan Password

read -s -p "Enter password: " password

# Mengambil Password Untuk Username Yang Di Masukkan Dari home/kali/soal3/users/users.txt

saved_password=$(grep -w -A 2 "$username" /home/kali/soal3/users/users.txt | sed -n 2p | cut -d " " -f 2)

# Cek Apabila Password Sudah Sama Seperti Yang Tersimpan Di Saved Password

if [ "$password" != "$saved_password" ]; then
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username "
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> /home/kali/soal3/log.txt
  exit 1
else
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in"
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> /home/kali/soal3/log.txt
  exit 0
fi
