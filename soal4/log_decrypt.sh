#!/bin/bash

# Mendapatkan waktu saat ini dan formatnya
date=$(date +"%H:%M %d:%m:%Y")
filename=$(date +"%H:%M %d:%m:%Y").txt

# Dekripsi isi file dengan string manipulation
while read line; do
    decrypted_line=""
    for (( i=0; i<${#line}; i++ )); do
        char=${line:i:1}
        if [[ $char =~ [a-zA-Z] ]]; then
            ascii_val=$(printf '%d' "'$char")
            if [[ $char =~ [a-z] ]]; then
                decrypted_val=$(( (ascii_val - 97 - ${#date} - 1 + 26) % 26 + 97 ))
            else
                decrypted_val=$(( (ascii_val - 65 - ${#date} - 1 + 26) % 26 + 65 ))
            fi
            decrypted_char=$(printf "\\$(printf '%03o' "$decrypted_val")")
            decrypted_line="$decrypted_line$decrypted_char"
        else
            decrypted_line="$decrypted_line$char"
        fi
    done
    echo "$decrypted_line" >> "${filename%.*}_decrypted.txt"
done < /home/kali/Desktop/$filename

echo "File $filename telah berhasil didekripsi."