#!/bin/bash

# Mendapatkan waktu saat ini dan formatnya
date=$(date +"%H:%M %d:%m:%Y")
filename=$(date +"%H:%M %d:%m:%Y").txt

# Enkripsi isi file dengan string manipulation
while read line; do
    encrypted_line=""
    for (( i=0; i<${#line}; i++ )); do
        char=${line:i:1}
        if [[ $char =~ [a-zA-Z] ]]; then
            ascii_val=$(printf '%d' "'$char")
            if [[ $char =~ [a-z] ]]; then
                encrypted_val=$(( (ascii_val - 97 + ${#date} + 1) % 26 + 97 ))
            else
                encrypted_val=$(( (ascii_val - 65 + ${#date} + 1) % 26 + 65 ))
            fi
            encrypted_char=$(printf "\\$(printf '%03o' "$encrypted_val")")
            encrypted_line="$encrypted_line$encrypted_char"
        else
            encrypted_line="$encrypted_line$char"
        fi
    done
    echo "$encrypted_line" >> /home/kali/Desktop/$filename
done < /var/log/syslog

echo "File syslog telah berhasil dicatat dan dienkripsi."

# comment perintah konfigurasi pada crontab
# crontab -e
# 0 */2 * * *      /home/kali/Desktop/log_encrypt.sh